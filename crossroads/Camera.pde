class SatanCamera {
 
    // Current paramters
    PVector camPos;
    PVector camLookAt;
    PVector camUp;
    
    // Inital parameters
    PVector camPosInitial;
    PVector camLookAtInitial;
    PVector camUpInitial; 
 
    // "following" parameters
    PVector camWhereItShouldBe = new PVector(0, 0, 0);
    PVector camAdd = new PVector(0, -10, 0);
    float   easing = .019; // .07; // how fast it changes
 
    // self/y-axis otations
    float camCurrentAngle=0;// -90;  
    float camRadius;
 
    PImage carWrapper;
    PImage steeringWheel;
    PImage musicalNote;

    public PMatrix viewMatrix;
    public PMatrix invViewMatrix;
    public PMatrix projMatrix;
    
    int contractAlpha = 0;

    // constructor without parameters
    SatanCamera(Game g) {
        camPos    = new PVector(width/2.0, height/2.0, 990);
        camLookAt = new PVector(width/2.0, height/2.0, -600);
        camUp     = new PVector( 0, 1, 0 );

        camPosInitial    = camPos.get();
        camLookAtInitial = camLookAt.get();
        camUpInitial     = camUp.get();
        carWrapper = loadImage("./data/integrateddash2.png");
        steeringWheel = loadImage("./data/steering1.png");
        musicalNote = loadImage("./data/note.png");
        game = g;
        
        viewMatrix = new PMatrix3D();
        invViewMatrix = new PMatrix3D();
        projMatrix = new PMatrix3D();
    }  // constructor

    void set() {
        // apply vectors to actual camera
        camera (camPos.x, camPos.y, camPos.z, 
                camLookAt.x, camLookAt.y, camLookAt.z, 
                camUp.x, camUp.y, camUp.z);
    }
    

    void update() {
        PVector vz = camPos.sub(camLookAt).normalize();
        PVector vx = new PVector(0,1,0).cross(vz).normalize();
        PVector vy = vz.cross(vx);
        viewMatrix.set(
            vx.x, vx.y, vx.z, 0,
            vy.x, vy.y, vy.z, 0,
            vz.x, vz.y, vz.z, 0,
            camPos.x, camPos.y, camPos.z, 1
        );
        invViewMatrix = viewMatrix.get();
        invViewMatrix.invert();
        projMatrix = ((PGraphics3D)g).modelview;
        if(contractAlpha > 0) {
          contractAlpha--;
        }
    }

    void setLookAt (float x1, float y1, float z1) { 
        camLookAt = new PVector(x1, y1, z1);
    }
    
 
    void report() {
        println ( "Cam at " + camPos 
        + " looking at " + camLookAt 
        + " (angle = "
        +camCurrentAngle
        +").");
    }
 
    void lookAtPVectorFollow (PVector followMe) {
        // follows an object    
        camLookAt =  followMe.get();
    
        float easing = 0.0198; 
        camPos.x    += (followMe.x-camPos.x) * easing;
        camPos.y    = followMe.y - 70;
        camPos.z    += (followMe.z-camPos.z) * easing;
    
        // printData();
    }
 
    void lookAtPVectorFollow2 (PVector followMe, float forwardAngle) {
        // follows an object    
        camLookAt =  followMe.get();
    
        float easing = 0.0198; 

        PVector tmp = new PVector(0, 90);
        tmp.rotate(forwardAngle);
        tmp = new PVector(tmp.x, 0, tmp.y);

        camPos.x    += (followMe.x-camPos.x) * 1 - tmp.x;
        camPos.y    = followMe.y - 30;
        camPos.z    += (followMe.z-camPos.z) * 1 - tmp.z; // - 90
        camCurrentAngle = forwardAngle;
        // printData();
    }

    void HUD_text (String a1) {
        // HUD text upper left corner 
        // this must be called at the very end of draw()
    
        // this is a 2D HUD 
        camera();
        hint(DISABLE_DEPTH_TEST);
        noLights();
        // ------------------
        textSize(16);
        text (a1, 20, 20);
        // text("push 1 to start", 20, 80);
        // rect(50, 60, 50, 60);
        // ------------------
        // reset all parameters to defaults
        textAlign(LEFT, BASELINE);
        rectMode(CORNER);
        textSize(32);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();

        //HUD_text2();
    } 

    void renderCarWrapper(float velX, int _px, int _pz, ArrayList<Musician> _blues) {
        // if I don't add the push and pop, this gets hilarious
        pushMatrix();
        camera();
        hint(DISABLE_DEPTH_TEST);
        noLights();

        game.map.drawMiniMap(_px, _pz, _blues);
        //image(musicalNote, 70, 160);
        image(carWrapper, 0, 0);
       

        pushMatrix();
            noStroke();
            float fuelRatio = Math.min((0.5f)*PI*(game.fuel/10000f), (8*PI/5));
            // println(fuelRatio);
            arc(283, 497, 45, 45, -6.5*PI/5, (-6.5*PI/5)+fuelRatio, PIE); // PI+QUARTER_PI
            fill(149);
            arc(283, 497, 40, 40, -6.5*PI/5, (-6.5*PI/5)+fuelRatio, PIE); // PI+QUARTER_PI
            arc(283, 497, 40, 40, 0, 2*PI, PIE); // PI+QUARTER_PI
        popMatrix();

        pushMatrix();
            noStroke();
            float maxSpeed = 130f;
            float speedRatio = Math.min((8*PI/5)*(game.car.speed/maxSpeed), (8*PI/5));
            // println(fuelRatio);


            fill( Math.min(100 + 155*(game.car.speed/maxSpeed), 255),0,0);

            arc(68, 497, 55, 55, -6.5*PI/5, (-6.5*PI/5)+speedRatio, PIE); // PI+QUARTER_PI
            fill(149);
            arc(68, 497, 40, 40, -6.5*PI/5, (-6.5*PI/5)+speedRatio, PIE); // PI+QUARTER_PI
            arc(68, 497, 40, 40, 0, 2*PI, PIE); // PI+QUARTER_PI
        popMatrix();

        stroke(255, 0, 0);

        pushMatrix();
            translate(177, 535);
            rotate(velX);
            translate(-steeringWheel.width/2, -steeringWheel.height/2);
        //translate(width/2, height/2);
            // rotate(90-camCurrentAngle);
            // translate(0, 340);
            image(steeringWheel, 0, 0);
        popMatrix();
    
        float scaledTalent = game.talent * 2;
        fill(100 + scaledTalent,0,0);
        rect(706, 472-scaledTalent, 39, scaledTalent);





        // image(img, 0, 0, width/2, height/2);
        // rectMode(CORNER);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();
        popMatrix();
    }

    void renderDebugParams(float velX) {
        pushMatrix();
        camera();
        hint(DISABLE_DEPTH_TEST);
        noLights();
        // ------------------
        textSize(16);
        text ("fuel: " + game.fuel, 20, 20);
        text ("score: " + game.score, 20, 40);
        text ("talent: " + game.talent, 20, 60);
        // text("push 1 to start", 20, 80);
        // rect(50, 60, 50, 60);
        // ------------------
        // reset all parameters to defaults
        textAlign(LEFT, BASELINE);
        rectMode(CORNER);
        textSize(32);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();
        popMatrix();
    }
    
    void renderContract() {
        pushMatrix();
        camera();
        hint(DISABLE_DEPTH_TEST);
        noLights();
        // ------------------
        textSize(20);
        fill(255, 255, 255, contractAlpha);
        text ("Contract signed", 300, 400);
        fill(255,255,255,255);

        textAlign(LEFT, BASELINE);
        rectMode(CORNER);
        textSize(32);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();
        popMatrix();
    }

} 