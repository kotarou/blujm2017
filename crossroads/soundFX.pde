import javax.sound.sampled.*;
import java.net.MalformedURLException;

final int SOUND_COLLISION = 0;
final int SOUND_COLLISION_PERSON = 1;
final int SOUND_GRAVEL = 2;
final int SOUND_IDEA = 3;
final int SOUND_SCREECH = 4;
final int SOUND_DEVIL_CACKLE = 5;
final int SOUND_DRIVE_FORWARD = 6;
final int SOUND_DRIVE_BACKWARDS = 7;
final int SOUND_AMBIENCE = 8;
final int SOUND_PAPERS = 9;
final int NUM_TRACKS = 10;

private class AudioPlayer 
{
  private Clip[] clip = new Clip[NUM_TRACKS];
  private String[] clips = {"/collision1.wav", "/collisionPerson.wav", "/DriveOffroad.wav", 
    "/IdeaSound.wav", "/wheelsScreechLoop.wav", "/DevilCackle.wav", 
    "/DriveBackwards.wav", "/DriveBackwards.wav", "/ambience.wav", 
    "/papersSigning.wav" };

  public AudioPlayer() {
    try {
      File[] file = new File[NUM_TRACKS];
      AudioInputStream[] sound = new AudioInputStream[NUM_TRACKS];

      for (int i = 0; i < NUM_TRACKS; i++) {
        file[i] = new File(dataPath(""), clips[i]);

        if (file[i].exists()) {
          sound[i] = AudioSystem.getAudioInputStream(file[i]);
          // load the sound into memory (a Clip)
          clip[i] = AudioSystem.getClip();
          clip[i].open(sound[i]);
        } else {
          throw new RuntimeException("Sound: file not found");
        }
      }
    }
    catch(Exception e) {
      System.out.println("Exception caught " + e.toString());
    }
  }

  boolean screechIsPlaying = false;
  boolean engineIsPlaying = false;
  public boolean ambiencePlaying = false;

  public void tick() {
    if (game.m_down && !screechIsPlaying) {
      play(SOUND_SCREECH, 0.05);
      loop(SOUND_SCREECH);  
      screechIsPlaying = true;
    } else if (!game.m_down && screechIsPlaying) {
      stop(SOUND_SCREECH);
      screechIsPlaying = false;
    }

    if (game.m_up && !engineIsPlaying) {
      play(SOUND_DRIVE_FORWARD, 0.15);
      loop(SOUND_DRIVE_FORWARD);
      engineIsPlaying = true;
    } else if (game.m_down && !engineIsPlaying) {
      play(SOUND_DRIVE_BACKWARDS, 0.1);
      loop(SOUND_DRIVE_BACKWARDS);
      engineIsPlaying = true;
    } else if ((!game.m_up && !game.m_down) && engineIsPlaying) {
      stop(SOUND_DRIVE_FORWARD);
      stop(SOUND_DRIVE_BACKWARDS);
      engineIsPlaying = false;
    }

    if (!ambiencePlaying && game.gameState == GAME) {
      play(SOUND_AMBIENCE, 0.1);
      loop(SOUND_AMBIENCE);
      ambiencePlaying = true;
    } else if (ambiencePlaying && game.gameState != GAME) {
      stop(SOUND_AMBIENCE);
      ambiencePlaying = false;
    }
    if (midiPlayer.currentSection != midiPlayer.SECTION_BREAKDOWN && game.fuel < 500) {
      midiPlayer.toBreakdown();
    }
    if (game.car.currentTerrain == "dirt" && game.gameState == GAME && (game.m_up || game.m_down)) {
      if (!isPlaying(SOUND_GRAVEL)) {
        play(SOUND_GRAVEL, 0.2);
        loop(SOUND_GRAVEL);
        println("GRAVELLLL");
      }
    }
    else {
      if (isPlaying(SOUND_GRAVEL)) {
        stop(SOUND_GRAVEL);
      }
    }
  }

  public void play(int clipToPlay, float gain) {
    final FloatControl control = (FloatControl) 
      clip[clipToPlay].getControl(FloatControl.Type.MASTER_GAIN);
    control.setValue(20.0 * (log(gain) / log(10)));
    clip[clipToPlay].setFramePosition(0);  // Must always rewind
    clip[clipToPlay].start();
  }



  public void loop(int clipToPlay) {
    clip[clipToPlay].loop(Clip.LOOP_CONTINUOUSLY);
  }
  public void stop(int clipToPlay) {
    clip[clipToPlay].stop();
  }
  public boolean isPlaying(int clipToCheck) {
    return(clip[clipToCheck].isRunning());
  }
}