class SatanCar {
 
    // Current paramters
    public PVector pos;
    public PVector vel;
    public PVector acc;
    
    public float forwardAngle;

    public float speed;

    public String lastTerrain = "dirt";
    public String currentTerrain     = "dirt";

     SatanCar() {
        pos = new PVector (0, 100, 10);
        vel = new PVector ( random(1, 2), 
                            0, 
                            random(-2, -1));
        acc = new PVector(0, 0, 0);

        // forwards = new PVector(0, 1);
        forwardAngle = radians(90f);
        speed = vel.mag();
    }  // constructor


    void update() {
        float frictionRatio = 0.98;
        float speedRatio = 1f;
        if (currentTerrain.equals("dirt")) {
            frictionRatio = 0.95;
            speedRatio = 0.8f;
        } if (lastTerrain.equals("dirt") && lastTerrain.equals("road")) {
            frictionRatio += 0.2;
            speedRatio += 0.2;
        }
        PVector tmp = new PVector(vel.x, vel.z);
        tmp.rotate(forwardAngle);
        tmp = new PVector(tmp.x, 0, tmp.y);
        pos = pos.add(tmp.mult(speedRatio));
        vel = vel.mult(frictionRatio);
        speed = vel.mag();
    }
 

} 