import javax.sound.midi.*;

private class MidiPlayer
{
  private String midiFiles[] = { "BLUESMUZAK.mid", "CheesyBossa.mid"
  };

  public final int          SECTION_HEAD = 0;
  public final int          SECTION_SOLO = 1;
  public final int          SECTION_BREAKDOWN = 2;
  public int                currentSection = SECTION_HEAD;
  
  private int               BPM;
  private File[]            file = null;

  private Track[]           track = null;
  private Sequence[]        sequence = null;
  private Sequencer[]       sequencer = null;
  private Synthesizer       synth;
  private MidiChannel[]     channels;

  public MidiPlayer() {
    file = new File[midiFiles.length];
    sequence = new Sequence[midiFiles.length];
    sequencer = new Sequencer[midiFiles.length];
    track = new Track[16];
    //***set bpm, ticks etc***//
    BPM = 80;
    try {
      for (int i = 0; i < midiFiles.length; i++) {
        file[i] = new File(dataPath(""), midiFiles[i]);
        sequence[i] = MidiSystem.getSequence(file[i]);
        track = sequence[0].getTracks();
        sequencer[i] = MidiSystem.getSequencer(false);
        sequencer[i].setSequence(sequence[i]);
        sequencer[i].open();
      }
      
      synth = MidiSystem.getSynthesizer();
      synth.open();
      Receiver synthReceiver = synth.getReceiver();
      for (int i = 0; i < midiFiles.length; i++) {
        Transmitter  seqTransmitter = sequencer[i].getTransmitter();
        seqTransmitter.setReceiver(synthReceiver);
      }

      channels = synth.getChannels();

      populateMidiBuffer();
    }
    catch(Exception e) {
      System.out.println("Exception caught " + e.toString());
    }

    sequencer[0].setTempoInBPM(BPM);
    sequencer[0].setLoopCount(sequencer[0].LOOP_CONTINUOUSLY);
    sequencer[1].setLoopCount(sequencer[1].LOOP_CONTINUOUSLY);
    sequencer[0].setLoopEndPoint(30720);

    sequencer[0].setTrackMute(0, true);
    //sequencer[0].setTrackMute(3, true);
    sequencer[0].setTrackMute(6, true);
    sequencer[0].setTrackMute(7, true);
    sequencer[0].setTrackMute(8, true);
  }// constructor

  private void addNote(Track track, int startTick, int tickLength, int key, int velocity)
    throws InvalidMidiDataException {
    ShortMessage on = new ShortMessage();
    on.setMessage(ShortMessage.NOTE_ON, 2, key, velocity);
    ShortMessage off = new ShortMessage();
    off.setMessage(ShortMessage.NOTE_OFF, 2, key, velocity);
    track.add(new MidiEvent(on, startTick));
    track.add(new MidiEvent(off, startTick + tickLength));
  }

  private int bluesScale(int fundimentalFreq) {
    int randomNote = (int)random(0, 7);
    int midiOut = 0;
    //67 is G
    switch (randomNote) {
    case 0:
      midiOut = fundimentalFreq;
      break;
    case 1:
      midiOut = fundimentalFreq + 3;
      break;
    case 2:
      midiOut = fundimentalFreq + 5;
      break;
    case 3:
      midiOut = fundimentalFreq + 6;
      break;
    case 4:
      midiOut = fundimentalFreq + 7;
      break;
    case 5:
      midiOut = fundimentalFreq + 10;
      break;
    case 6:
      midiOut = fundimentalFreq + 12;
      break;
    }
    return midiOut;
  }

  public void toSoloSection() {
    populateMidiBuffer();

    sequencer[0].setTrackMute(0, false);
    sequencer[0].setTrackMute(1, true);
    sequencer[0].setTrackMute(2, true);
    sequencer[0].setTrackMute(3, false);
    sequencer[0].setTrackMute(4, false);
    sequencer[0].setTrackMute(5, false);
    sequencer[0].setTrackMute(6, true);
    sequencer[0].setTrackMute(7, true);
    sequencer[0].setTrackMute(8, true);
    currentSection = SECTION_SOLO;
  }
  public void toHead() {
    sequencer[0].setTrackMute(0, true);
    sequencer[0].setTrackMute(1, false);
    sequencer[0].setTrackMute(2, false);
    sequencer[0].setTrackMute(3, false);
    sequencer[0].setTrackMute(4, false);
    sequencer[0].setTrackMute(5, false);
    sequencer[0].setTrackMute(6, true);
    sequencer[0].setTrackMute(7, true);
    sequencer[0].setTrackMute(8, true);
    currentSection = SECTION_HEAD;
  }
  public void toBreakdown() {
    populateMidiBuffer();
    sequencer[0].setTrackMute(0, false);
    sequencer[0].setTrackMute(1, true);
    sequencer[0].setTrackMute(2, true);
    sequencer[0].setTrackMute(3, true);
    sequencer[0].setTrackMute(4, true);
    sequencer[0].setTrackMute(5, true);
    sequencer[0].setTrackMute(6, false);
    sequencer[0].setTrackMute(7, false);
    sequencer[0].setTrackMute(8, false);
    currentSection = SECTION_BREAKDOWN;
  }
  private void populateMidiBuffer() {
    try {
      int eventsInSolo = track[0].size();
      for (int i = eventsInSolo - 1; i >= 0; i--) {
        track[0].remove(track[0].get(i));
      }

      for (int i = 0; i < 320 * 3 * 4 * 8; i+= 320) {
        int noteGo = (int)random(7);
        if (noteGo == 1 || noteGo == 2 || noteGo == 3) {
          addNote(track[0], i, 320, bluesScale(67), 127);
        }
        if (noteGo == 4) {
          addNote(track[0], i, 960, bluesScale(67), 127);
          i+= 640;
        }
      }
    }
    catch(Exception e) {
      System.out.println("Exception caught " + e.toString());
    }
  }
  public void setInstrument(int instrument) {
    channels[2].programChange(instrument);
  }
  public void playMenuMusic() {
    midiPlayer.sequencer[1].start();
    midiPlayer.sequencer[0].stop();
  }
    public void playGameMusic() {
    midiPlayer.sequencer[0].start();
    midiPlayer.sequencer[1].stop();
  }
}