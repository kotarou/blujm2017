private class Map
{

  String[] rawData;
  PImage texture, treeTex;

  public ArrayList<PVector> blues;
  public ArrayList<PVector> trees;

  int halfWorldSize = 50000; // 50000
  int squareSize = 1000;

  public Map() {
    rawData = loadStrings("./data/map2.txt");
    blues = new ArrayList<PVector>();
    trees = new ArrayList<PVector>();

    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        String typ = getTileType(i, j) ;
        if (typ.equals("blue")) {
          blues.add(new PVector(i, j));
        } else if (typ.equals("tree")) {
          trees.add(new PVector(i, j));
        }
      }
    }

    texture = loadImage("./PathAndObjects.png");
    //treeTex = loadImage("./_tree_10_00000.png");
  }

  private String getTileType(int x, int y) {
    // println(rawData[0]);
    // println(x + " " + y);
    String line = rawData[y];
    char type = line.charAt(x);

    if (type == '#') {
      return "road";
    } else if (type == '.') {
      return "dirt";
    } else if (type == '0') {
      return "blue";
    } else if (type == 't') {
      return "tree";
    }


    return "dirt";
  }

  public void draw() {
    noStroke();
    noFill();
    textureWrap(REPEAT);
    textureMode(IMAGE);
    fill(255, 255, 255);
    for (int i = 0; i < halfWorldSize * 2 / squareSize; i++) {
      for (int j = 0; j < halfWorldSize * 2 / squareSize; j++) {
        drawTile(i, j);
      }
    }
  }

  private void drawTile(int x, int y) {
    String typ = getTileType(x, y);
    int t = 1, b = 0, l = 0, r = 1;

    // if (i == 25 || i == (halfWorldSize * 2 / squareSize) - 25) {
    //     road = true;
    // }
    // if (j == 25 || j == (halfWorldSize * 2 / squareSize) - 25) {
    //     road = true;
    // }

    if (typ.equals("road")) {
      t = 256;
      l = 87;
      r = 192;
      b = 319;
      fill(240, 240, 240);
    } else { // dirt or tree or blue
      t = 96;
      b = 159;
      l = 192;
      r = 287;
      // fill(165,42,42);
      fill(139, 69, 19);
    }


    beginShape(QUADS);
    //texture(texture);
    vertex(-halfWorldSize + (squareSize*x), 0, -halfWorldSize + (squareSize*y), l, b); // BL
    vertex(-halfWorldSize + (squareSize*x), 0, -halfWorldSize + (squareSize*(y+1)), l, t); // TL
    vertex(-halfWorldSize + (squareSize*(x+1)), 0, -halfWorldSize + (squareSize*(y+1)), r, t); // TR
    vertex(-halfWorldSize + (squareSize*(x+1)), 0, -halfWorldSize + (squareSize*y), r, b); // BR
    endShape();

    // renmdering of blues now done in other loop.
    if (typ.equals("blue")) {
      // RENDER THE BLUES
    }
  }

  public void drawMiniMap(int _px, int _pz, ArrayList<Musician> _blues) {
    int dx = 636;
    int dz = 399;

    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        if (abs(i - _px) < 20 && abs(j - _pz) < 20) {
          String typ = getTileType(i, j);

          int x = 3 * (i - _px);
          int z = 3 * (j - _pz);


          if (typ.equals("road")) {
            stroke(0, 0, 0);
            rect(x + dx, z + dz, 3, 3);
          }
          // if (typ.equals("blue")) {
          //   stroke(255, 0, 0);
          //   point(x + dx, z + dz);
          // }
        }
      }
    }

    for(Musician p : _blues) {
      if (abs(p.x - _px) < 20 && abs(p.z - _pz) < 20 && p.alive) {
          int x = 3 * (p.x - _px);
          int z = 3 * (p.z - _pz);

          stroke(p.pactPercentage * 2.55, 255, 255);
          // point(x + dx, z + dz);
          rect(x+dx-1, z+dz-1,3,3);
      }

    }

    stroke(255, 0, 0);
    fill(255, 0, 0);
    rect(dx, dz, 5, 5);
  }
}