private class Musician
{

  int x;
  int z;

  float talent;
  float worth;
  int age;
  int maxAge;

  int pactPercentage;

  boolean alive;
  boolean devilled;

  float earningFactor = 0.15;

  public int colour = 0;

  public Musician(int _x, int _z) {
    x = _x;
    z = _z;

    talent  = random(1f, 2f);
    worth   = 500;
    age     = 0;
    maxAge  = (int)random(15*60, 150*60);

    alive = true;
    pactPercentage = 0;
    if (random(0f, 1f) > 0.5f) {
      colour = 1;
    }
  }

    public void reset() {
        talent  = random(1f, 2f);
        worth   = 500;
        age     = 0;
        maxAge  = (int)random(15*60, 350*60);

        alive = true;
        pactPercentage = 0;
        if (random(0f, 1f) > 0.5f) {
            colour = 1;
        }
    }

  public void update() {
    age += 1;
    if (age > maxAge) {
      alive = false;
    }

    if (devilled) {
        talent += 0.00001;
        worth += (talent * 2) * earningFactor;
    }

    if(worth > 3000) {
        worth = 3000;
    }
    game.cam.renderContract();
  }

    public boolean pact() {
        if (!devilled && alive) {
            pactPercentage++;
            if(pactPercentage == 1) {
              if(!audioPlayer.isPlaying(SOUND_PAPERS)) {
                audioPlayer.play(SOUND_PAPERS, 0.8);
              }
            }
            if(pactPercentage == 99 && !audioPlayer.isPlaying(SOUND_DEVIL_CACKLE)) {
                audioPlayer.play(SOUND_DEVIL_CACKLE, 0.8);
            }
            if(pactPercentage >= 100) {
                audioPlayer.stop(SOUND_PAPERS);
                devilled = true;
                talent += 1;
                if (midiPlayer.currentSection == midiPlayer.SECTION_HEAD) {
                    midiPlayer.toSoloSection();
                }
                game.cam.contractAlpha = 105;
                return true;
            }
        }
        return false;
    }

  public float collide() {
    alive = false;
    audioPlayer.play(SOUND_COLLISION_PERSON, 0.5);
    audioPlayer.stop(SOUND_PAPERS);
    midiPlayer.toHead();
    return worth;
  }

  public boolean nearBy(int _x, int _z) {
    return alive && abs(_x - x) < 2 && abs(_z - z) < 2;
  }
}