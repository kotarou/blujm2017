private class Game
{

    private float dt = 1 / FRAMERATE;

    PVector focusPos = new PVector (0, 40, -100);
    SatanCamera cam;
    SatanCar car;
    Map map;


    // Other
    public int playerHeight = 40; //standHeight
    public float fudge = 1f;

    // movement variables
    public float velocityScale = 1f / 50f ;
    public float accleractionFactor = 2.3f;

    // Input
    public boolean m_up, m_down, m_left, m_right;
    public int moveX, moveZ;
    
    // settings 
    private int motionType = 2; // 0 - position, 1 - velocity, 2 - accleration

    //Constants
    int time;
    float rot_y;

    final int INTRO = 0;
    final int MAIN_MENU = 1;
    final int GAME = 2;
    final int GAMEOVER_LOSE = 3;
    final int GAMEOVER_WIN = 4;
    final int HIGH_SCORES = 5;
    final int SETTINGS = 6;
    
    
    public int gameState = 1;

    // Game values
    public int fuel = 10000;
    public int score = 0;
    public float talent = 1;
    
    PImage bluesTextureB, bluesTextureW, shadowTexture;


    public ArrayList<Musician> blues;

    public Game() {
        // Setup camera
        cam = new SatanCamera(this);
        car = new SatanCar();
        
        map = new Map();

        cam.camPos = car.pos.copy();
        cam.camPos.x += 13;
        cam.camPos.y -= 63;
        cam.camPos.z += 13;

        m_up = false; m_down = false; m_left = false; m_right = false;
        time = 0;
        
        bluesTextureB = loadImage("./data/bluesmanb.png");
        bluesTextureW = loadImage("./data/bluesmanw.png");
        shadowTexture = loadImage("./data/shadow.png");

        blues = new ArrayList<Musician>();
        for( PVector blue : map.blues) {
            if (random(0f,1f) > 0.7f) {
                blues.add(new Musician((int)blue.x, (int)blue.y));
            }
        }

    }

    public void tick() {
        // Spawn a number of new musicians
        if(time % 600 == 0) {
            boolean notFound = true;
            while(notFound) {
                int index = (int)random(0, map.blues.size());
                PVector pos = map.blues.get(index);
                boolean found = false;
                for( Musician blue : blues) {
                    if (blue.x == pos.x && blue.z == pos.y) {
                        if (blue.alive) {
                            notFound = false;
                            found = true;
                            break;
                        } else {
                            found = true;
                            blue.reset();
                        }
                    }
                }
                if (!found) {
                    Musician prop = new Musician((int)pos.x, (int)pos.y);
                    notFound = false;
                    blues.add(prop);
                }

            }
            println("Spawning!");
         }

        cam.update();
        // clear canvas 
        background(111);
        
        // clipping
        perspective(PI/3.0, (float) width/height, 1, 1000000);

              int px = (int)(car.pos.x + map.halfWorldSize) / map.squareSize;
            int pz = (int)(car.pos.z + map.halfWorldSize) / map.squareSize;

            String type = map.getTileType(px, pz);
            car.currentTerrain = type;
            

        updateObjects();
        updatePlayer();

        lights();
   
        //Draw Boxes

        pushMatrix();
           translate(0, height/2, 0);
            ////////////////////////////////////////
            // Ground tiles
            map.draw();

            ////////////////////////////////////////
            // Some buildings
            // cam.update();
            

            int hw = map.halfWorldSize;
            int ss = map.squareSize;
           // fill(0, 0, 0);
            for( PVector tree : map.trees) {
                pushMatrix();
                    translate(-hw + (ss*tree.x) + 500, 0, -hw + (ss*tree.y) + 500);
                    applyMatrix(cam.invViewMatrix);
                    textureMode(NORMAL);
                    fill(0,0,0);
                    beginShape(QUADS);
                        vertex( -500f, -500, 0, 0, 0);
                        vertex( -500f, 500f, 0, 0, 1);
                        vertex( 500f, 500f,  0, 1, 1);
                        vertex( 500f, -5000, 0, 1, 0);
                    endShape();
                //box(10, 10, 10);
                popMatrix();
            }

            fill(255, 0, 0);
            for( Musician blue : blues) {
                blue.update();
                if (!blue.alive) {
                    continue;
                }
                pushMatrix();
                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////
                // This method renders the musicians
                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                translate(-hw + (ss*blue.x) + 500, -370, -hw + (ss*blue.z) + 500);
                
                // if (blue.devilled) {
                // pushMatrix();
                //     translate(-40, 360, 40);
                //     beginShape(QUADS);     
                //     textureMode(NORMAL);        
                //     texture(shadowTexture);
                //     int t = 0;
                //     int b = -20;

                //     int zt = 100;
                //     int zb = -100;
                //     int xt = 100;
                //     int xb = -100;
                //     vertex( xb, t, zb, 0, 0);
                //     vertex( xb, t, zt, 0, 1);
                //     vertex( xt, t,  zt, 1, 1);
                //     vertex( xt, t, zb, 1, 0);
                //     endShape();
                // popMatrix();
                // }


                applyMatrix(cam.invViewMatrix);
                textureMode(IMAGE);
                textureWrap(CLAMP); // repeat
                beginShape(QUADS);

                int height = 300;

                if(blue.devilled) {
                    fill(255,0,0);
                } else {
                    fill(blue.pactPercentage * 2.55, 120 - (blue.pactPercentage * 1.2), 120 - (blue.pactPercentage * 1.2));
                }

                if (blue.colour == 0) {
                    texture(bluesTextureB);
                } else {
                    texture(bluesTextureW);
                }

                vertex( -100f, -height + 120, 0, 0, 0);
                vertex( -100f, height + 120, 0, 0, 400);
                vertex( 100f, height + 120,  0, 300, 400);
                vertex( 100f, -height + 120, 0, 300, 0);
                endShape();




                popMatrix();
            }




    

            // Manage game logic from here because I am bad.


            float fuelSpendScale = 150;

            if (type.equals("tree")) {
                println(car.pos.x + " " + car.pos.z + " " + px + " " + pz);
                this.gameEnd();
                // exit();
            }    
            
            if (type.equals("road")) {
                fuelSpendScale = 20;
            }

            if (type.equals("blue")) {
                for (Musician p : blues) {
                    if (p.x == px && p.z == pz && p.alive) {
                        println("Making a collision @ " + px + " " + pz);
                        fuel += p.collide();
                        talent += p.talent;
                        break;
                    }
                }
            } 


            //ArrayList<Musician> nearBy = new ArrayList<Musician>();
            for (Musician p : blues) {
                if (p.nearBy(px, pz)) {
                    //nearBy.add(p);
                    boolean progress = p.pact();
                    if (progress && talent > 1) {
                        println("Pact complete @ " + px + " " + pz);
                        // talent -= 1;
                    } else if (talent > 1 && !p.devilled) {
                        println("Making a pact @ " + px + " " + pz);
                    }
                }
            }

            fuel -= (car.speed * fuelSpendScale * 0.002);
            if (Math.round(car.speed) == 0) {
                fuel -= 1;
            }
            if (fuel <= 0) {
                this.gameEnd();
                // exit();
            }

        popMatrix();

        // renderSphere(car.pos.x, car.pos.y, car.pos.z, 1);
    
        cam.lookAtPVectorFollow2(car.pos, car.forwardAngle); 
        cam.set();

        time++;
        score += (talent / 5f);
        talent -= 0.001;
        if (talent < 1) {
            talent = 1;
        }
        // cam.HUD_text(px + " " + pz + " " + type);
        cam.renderCarWrapper(rot_y, px, pz, blues);
        cam.renderDebugParams(rot_y);
        rot_y *= 0.8;
        car.lastTerrain = type;
    }

    public void updateObjects() {
        car.update();
    } // updateCamera()

    public void updateCamera() {

    } // updateCamera()


    public void updatePlayer(){
        if (this.m_up) {
            car.vel.z = car.vel.z + 2.4;
        } 
        if (this.m_down) {
            car.vel.z = car.vel.z - 2.4;
        } 
        if (this.m_left) {
            // car.vel.x += 0.9;
            rot_y -= 0.15;
            car.forwardAngle -= 0.035; //radians(3);
        } 
        if (this.m_right) {
            // car.vel.x -= 0.9;
            rot_y += 0.15;
            car.forwardAngle += 0.035; //radians(3);
        }        
    }



    public void renderSphere(float x, float y, float z, float size1) {
        pushMatrix();
        translate(x, y, z);
        rotateY(radians(45));
        rotateX(radians(45));
        sphere(size1);
        popMatrix();
    }

    
    // Initialises fuel and score and other game specific variables
    public void initialiseGame(){
      this.fuel = 1000;
      this.score = 0;
      gameState = GAME;
    }
    
    public void gameEnd(){
      Table table = loadTable("./data/highscores.csv");
      TableRow newRow = table.addRow();
      newRow.setString(0, "Punter");
      newRow.setInt(1, this.score);
      saveTable(table, "./data/highscores.csv");
      gameState = HIGH_SCORES; 
    }

}