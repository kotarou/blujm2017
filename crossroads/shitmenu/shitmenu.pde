
final int INTRO = 0;
final int MAIN_MENU = 1;
final int GAME = 2;
final int GAMEOVER_LOSE = 3;
final int GAMEOVER_WIN = 4;
final int HIGH_SCORES = 5;
final int SETTINGS = 6;

PFont title;
PFont menu;
int gamestate = 1;


void setup() {
  size(800, 600);
  title = createFont("Arial",40,true);
  menu = createFont("Arial",20,true);
}

void draw() {
  switch(gamestate) {
    case MAIN_MENU:
       background(255,255,255);
       textFont(title);
       fill(0, 0, 0);
       text("CROSSROADS", 50, 50);
       textFont(menu);
       text("1 - New Game", 50, 90);
       text("2 - High Scores", 50, 120);
       text("3 - Settings", 50, 150);
       text("4 - Exit", 50, 180);
       break;
       
    case HIGH_SCORES:
       background(255,255,255);
       textFont(title);
       text("HIGH SCORES", 50, 50);
       textFont(menu);
       text("No high scores, 1 to return to main menu", 50, 90);
  }
  
 }
 
 void keyPressed() {
   switch(gamestate) {
     case MAIN_MENU:
       if (key == '1'){
         gamestate = GAME;
       }
       else if (key == '2'){
         gamestate = HIGH_SCORES;
       }
       else if (key == '3'){
         gamestate = SETTINGS;
       }
       else if (key == '4'){
         exit();
       }
       break;
    case HIGH_SCORES:
      if (key == '1'){
         gamestate = MAIN_MENU;
       }
   }
 }
 
 
 
 /*
 void mousePressed() {
   // this is pretty hacky, but I dunno the processingonic way
   switch(gamestate) {
    case MAIN_MENU:
      if (mouseX > 50 && mouseX < 70 && mouseY > 120 && mouseY < 140){
        gamestate = HIGH_SCORES;
      }
   }
   
 }*/