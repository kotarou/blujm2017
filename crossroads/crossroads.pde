/**
* Critical configuration.
*
*/
public final int FRAMERATE  = 60;
// public final int W_WIDTH    = 800;
// public final int W_HEIGHT   = 600;

final int INTRO = 0;
final int MAIN_MENU = 1;
final int GAME = 2;
final int GAMEOVER_LOSE = 3;
final int GAMEOVER_WIN = 4;
final int HIGH_SCORES = 5;
final int SETTINGS = 6;


/**
* Setup and declare variables for use in the program.
*
*/ 
MidiPlayer midiPlayer = null;
AudioPlayer audioPlayer = null;
Game game;

void setup() {
  frameRate(FRAMERATE);
  size(800, 600, P3D);

  game = new Game();

  midiPlayer  = new MidiPlayer();
  audioPlayer = new AudioPlayer();
  midiPlayer.playMenuMusic();
}
void draw(){

  switch(game.gameState) {
      case GAME:
        game.tick(); 
        midiPlayer.playGameMusic();
        break;
      case MAIN_MENU:
        renderMainMenu();
        midiPlayer.playMenuMusic();
        break;
      case HIGH_SCORES:
        renderHighScores();
        midiPlayer.playMenuMusic();
        break;        
  }
   
  audioPlayer.tick();
}


public void renderMainMenu() {
          // HUD text upper left corner 
        // this must be called at the very end of draw()
    
        background(0);

        // this is a 2D HUD 
        pushMatrix();
        hint(DISABLE_DEPTH_TEST);
        noLights();
        

       textSize(40);
       text("CROSSROADS", 50, 50);
       textSize(16);
       text("Use the number keys to select", 50, 90);
       text("1 - New Game", 50, 120);
       text("2 - High Scores", 50, 150);
       text("3 - Exit", 50, 180);
       
        // ------------------
        // reset all parameters to defaults
        textAlign(LEFT, BASELINE);
        rectMode(CORNER);
        textSize(32);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();
        popMatrix();
}

public void renderHighScores() {
        // HUD text upper left corner 
        // this must be called at the very end of draw()
        // this is a 2D HUD 
        background(0);
        pushMatrix();
        hint(DISABLE_DEPTH_TEST);
        noLights();

        // ------------------
       textSize(40);
       text("High Scores", 50, 50);
       textSize(16);
       text("Hit 1 to return to main menu", 50, 80);
       Table table = loadTable("./data/highscores.csv");
       table.setColumnType(1, "int");
       table.sortReverse(1);
       int i = 0;
       for (TableRow row : table.rows()) { //<>// //<>//
         if (i < 5){
           text("" + row.getString(0) + " " + row.getString(1) , 50, 120 + (i*20));
           i++; //<>//
         }
       }
        // ------------------
        // reset all parameters to defaults
        textAlign(LEFT, BASELINE);
        rectMode(CORNER);
        textSize(32);
        hint(ENABLE_DEPTH_TEST); // no HUD anymore
        lights();
        popMatrix();
}


public void keyPressed(){
  // key presses change overall state 
  
  switch(game.gameState) {
    case GAME:
      if(keyCode == UP || key == 'w'){
        game.m_up = true;
      }  else if(keyCode == DOWN || key == 's'){
        game.m_down = true;
      }  else if(keyCode == LEFT || key == 'a'){
        game.m_left = true;
      }  else if(keyCode == RIGHT || key == 'd'){
        game.m_right = true;
      }
      break;
      
    case MAIN_MENU:
      if (key == '1'){
         game.gameState = GAME;
       }
       else if (key == '2'){
         game.gameState = HIGH_SCORES;
       }
       else if (key == '3'){
         exit();
       }
       break;
       
    case HIGH_SCORES:
      if (key == '1'){
         game.gameState = MAIN_MENU;
       }
    }
}

public void keyReleased(){
  switch(game.gameState) {
    case GAME:
      if(keyCode == UP || key == 'w'){
          game.m_up = false;
      }  else if(keyCode == DOWN || key == 's'){
          game.m_down = false;
      }  else if(keyCode == LEFT || key == 'a'){
          game.m_left = false;
      }  else if(keyCode == RIGHT || key == 'd'){
          game.m_right = false;
      }
      break;
  }
}